<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress1');

/** MySQL database username */
define('DB_USER', 'wpuser1');

/** MySQL database password */
define('DB_PASSWORD', 'asdf123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L}=-=dy]KXvb7?fAdQQM^0K#)>l|VezHIFJq=>87!fiUL0ZN4sx*D$|N+u5BN~g^');
define('SECURE_AUTH_KEY',  '!*AkG#9nI_T}qU!Pjwk77Q$g(DVJA|jH*>-9sV|GU!sO|vLg9`O6=-rK&zP16T9P');
define('LOGGED_IN_KEY',    'VfNrfw?&J*K|l!vS0>~U]LY63!P7{r7a0 R]277ho)`:3xfBOad;,4qQhX<v<EbP');
define('NONCE_KEY',        'B:cX-j,Za6-Kh`U+apx_J,T~{@JmdH^OsgmOuA|Qn7o-Swq!WT)0YKr~>|~mj*rT');
define('AUTH_SALT',        'LiqRUaj&Ft}|hOl*{N(_[=j%?DJqpZoOj{)~tYP|%+ RkL_]|.?y^-h,|FGRAidR');
define('SECURE_AUTH_SALT', 'nWtLu.E:0ZhmkA,zXkpYB39vU1a)]a=|n0I[+-=VO|g0isdGaUDP|%Kdl<6k8:g`');
define('LOGGED_IN_SALT',   '7EqpdNHUEY-C9z4m}se+C]h%.pV1TMbKz|o7+&n-B0.kxw.ih%CD7|jT|AEy0qB<');
define('NONCE_SALT',       'Jp2iZTZR0.Yb-)ec//,dZCufZ|-Ed8,U-44P%^PWq/s~`]-P?++-7q)eJsKG p*#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
